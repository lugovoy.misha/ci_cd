FROM ubuntu:18.04

WORKDIR /usr/src/app

RUN apt-get update \
  && apt-get -y install curl gnupg \
  && curl -sL https://deb.nodesource.com/setup_13.x  | bash - \
  && apt-get -y install nodejs \
  && npm install -g cowsay

COPY package.json /usr/src/app/package.json
COPY package-lock.json /usr/src/app/package-lock.json

RUN npm install
COPY . /usr/src/app/

CMD cowsay -f dragon "I am alive:) Mu-u-u-u..." && npm start
