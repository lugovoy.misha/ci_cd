# Docker image
 - create image from ubuntu
 - setup node latest, cowsay, node_modules
 - move progect to image

# Docker container
 - check that the image is being assembled and the container is launched from it
 - if you open on a localhost on a given port - your site should appear
 - in -it mode there should be a message in the console - _I am alive:) Mu-u-u-u..._

# Gitlab CI/CD
 - configure gitlab CI to execute the build of your container and push into _gitlab regestry_ with tag _latest_ and short commit hash.
 example - ```cuda:latest, cuda:fh56g```

# Host actions(creating bash script)
### On host mashine write a bash script: 
 - stop docker container
 - remove container
 - remove image
 - pull new image
 - run container 