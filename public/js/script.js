function showMenu() {
  document.getElementById("toggle").classList.toggle("on");
  let headerModalBox = document.getElementById("header-modal");

  headerModalBox.classList.toggle("show-mobile-navmenu");
  headerModalBox.firstElementChild.classList.toggle("show-menu-box");
}
