(function() {
  if (!document.getElementsByClassName("searcher")[0]) {
    let element = null;
    const widget = createSearcher();
    const btns = [
      {
        name: "prevBtn",
        elemName: "previousElementSibling",
        elem: null,
      },
      {
        name: "nextBtn",
        elemName: "nextElementSibling",
        elem: null,
      },
      {
        name: "parentBtn",
        elemName: "parentElement",
        elem: null,
      },
      {
        name: "childrenBtn",
        elemName: "firstElementChild",
        elem: null,
      },
    ];

    widget.addEventListener("click", e => {
      let btn = e.target.tagName == "BUTTON" ? e.target : null;
      if (btn) {
        const btnClassName = btn.classList[btn.classList.length - 1];
        highlight(element);
        element = searchElement(btnClassName, element);
        element.scrollIntoView();
        highlight(element);
        addSubElements(btns, element);
        toggleBtnsStatus(btns);
      }
    });

    const closeBtn = document.getElementsByClassName("s-header__close-btn")[0];
    closeBtn.addEventListener("click", () => {
      widget.remove();
      highlight(element);
    });

    function searchElement(btnClassName, el) {
      if (btnClassName == "searchBtn") {
        el = document.querySelector(
          document.getElementsByClassName("main-box__input")[0].value
        );
      } else {
        btns.forEach(bt => {
          if (bt.name == btnClassName) {
            el = bt.elem;
          }
        });
      }
      return el;
    }

    function highlight(el) {
      if (el != null) {
        el.classList.toggle("highlight");
      }
    }

    function addSubElements(btns, el) {
      btns.forEach(btn => (btn.elem = el[btn.elemName]));
    }

    function toggleBtnsStatus(btns) {
      btns.forEach(btn => {
        const wBtn = document.getElementsByClassName(btn.name)[0];
        btn.elem ? (wBtn.disabled = false) : (wBtn.disabled = true);
      });
    }

    let mouseCoordsIntoDiv = { x: 0, y: 0 };

    const searcher = document.getElementsByClassName("searcher")[0];
    searcher.addEventListener("dragstart", dragStart);
    searcher.addEventListener("dragend", dragEnd);
    function dragStart(e) {
      let coords = getCoords(searcher);
      mouseCoordsIntoDiv.x = e.pageX - coords.left;
      mouseCoordsIntoDiv.y = e.pageY - coords.top;
    }

    function dragEnd(e) {
      e.target.style.left = e.pageX - mouseCoordsIntoDiv.x + "px";
      e.target.style.top = e.pageY - mouseCoordsIntoDiv.y + "px";
    }

    function getCoords(elem) {
      let box = elem.getBoundingClientRect();
      return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset,
      };
    }

    function createSearcher() {
      const widget = document.createElement("div");

      widget.innerHTML = `
      <div class="searcher" draggable="true">
        <div class="searcher__wrap wrap">
          <div class="searcher__header s-header">
            <h4 class="s-header__title">Search node element</h4>
            <div class="s-header__close-btn">&times;</div>
          </div>
          <div class="searcher__main main-box">
            <input type="text" class="main-box__input">
            <button class="main-box__btn searcher-btn searchBtn">Search</button>
          </div>
          <div class="searcher__btn-group ">
            <ul class="buttons-list">
              <li class="buttons-list__wrap"><button class="buttons-list__btn searcher-btn prevBtn" disabled>Prev</button></li>
              <li class="buttons-list__wrap"><button class="buttons-list__btn searcher-btn nextBtn" disabled>Next</button></li>
              <li class="buttons-list__wrap"><button class="buttons-list__btn searcher-btn parentBtn" disabled>Parent</button></li>
              <li class="buttons-list__wrap"><button class="buttons-list__btn searcher-btn childrenBtn" disabled>Children</button></li>
            </ul>
          </div>
        </div>
      </div>
      `;

      const styles = document.createElement("style");
      styles.innerHTML = `

      .searcher {
        position: absolute;
        width: 300px;
        height: 128px;
        background-color: grey;
        color: #fff;
        top: 0;
        right: 0;
        border-radius: 5px;
        z-index: 10000;
      }

      .searcher__wrap {
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: 10px;
      }

      .searcher__header {
        display: flex;
        width: 100%;

        justify-content: space-between;
        align-items: center;
        margin-bottom: 10px;
      }

      .s-header__title {
        margin: 0;
      }

      .searcher__main {
        display: flex;
        width: 100%;
        color: grey;
        height: 32px;
        margin-bottom: 10px;
      }

      .s-header__close-btn:hover {
        cursor: pointer;
      }

      .main-box__input {
        width: 300%;
        padding: 0;
        padding-left: 5px;
        margin-right: 5px;
        border: none;
        font-size: 18px;
      }

      .searcher__btn-group {
        width: 100%;
      }

      .buttons-list {
        display: flex;
        justify-content: space-between;
        width: 100%;
        list-style: none;
        margin: 0;
        padding:0;
      }

      .searcher-btn {
        color: #5f5e5e;
        border: 1px solid #4f4e4e;
        border-radius: 5px;
        padding: 4px 12px;
        width: 100%;
        background-color: #ddd;
      }

      .searcher-btn:hover {
        background-color: #5f5e5e;
        color: #ddd;
      }

      .searcher-btn:disabled {
        pointer-events:none;

        color: #b2b0b0;
      }

      .highlight {
        border: 1px solid red;
      }

      `;
      document.body.appendChild(widget);
      document.head.appendChild(styles);
      return widget;
    }
  }
})();
